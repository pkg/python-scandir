python-scandir (1.10.0-4apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 21:22:26 +0000

python-scandir (1.10.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Julien Puydt ]
  * Update lintian overrides.
  * Declare d/rules doesn't require root.
  * Bump dh compat to 13.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 13 Oct 2020 08:33:16 +0200

python-scandir (1.10.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938156

 -- Sandro Tosi <morph@debian.org>  Wed, 15 Apr 2020 00:59:53 -0400

python-scandir (1.10.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 28 Jul 2019 08:57:38 +0200

python-scandir (1.10.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Rename d/tests/control.autodep8 to d/tests/control.

  [ Julien Puydt ]
  * New upstream release.
  * Bump std-vers to 4.3.0.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 22 Apr 2019 09:42:25 +0200

python-scandir (1.9.0-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:52:27 +0000

python-scandir (1.9.0-2) unstable; urgency=medium

  * Team upload.
  * Enable autopkgtest-pkg-python testsuite
  * Add pypy-scandir binary package
  * Standards version is 4.2.1 now (no changes)

 -- Ondřej Nový <onovy@debian.org>  Sat, 03 Nov 2018 12:37:17 +0100

python-scandir (1.9.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * d/changelog: Remove trailing whitespaces

  [ Julien Puydt ]
  * New upstream release.
  * Bump std-ver to 4.2.0.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 14 Aug 2018 22:53:53 +0200

python-scandir (1.8-1) unstable; urgency=medium

  * New upstream release.
  * Refresh packaging:
    - Update dates in d/copyright.
    - Use my debian.org mail address.
    - Bump dh compat to 11.
    - Bump std-ver to 4.1.5.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 06 Aug 2018 07:41:06 +0200

python-scandir (1.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one

  [ Julien Puydt ]
  * New upstream release.
  * Bump std-ver to 4.1.3.

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 15 Feb 2018 13:58:00 +0100

python-scandir (1.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump std-ver to 4.1.1.
  * Bump d/watch to version 4.
  * Add upstream testsuite to autopkgtest.

 -- Julien Puydt <julien.puydt@laposte.net>  Sun, 01 Oct 2017 09:55:36 +0200

python-scandir (1.5-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 21 Apr 2017 14:40:15 +0200

python-scandir (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #711388)

 -- Julien Puydt <julien.puydt@laposte.net>  Sat, 14 Jan 2017 12:34:06 +0100
